package ru.teterin.tm.enumerated;

public enum Role {

        USER("USER"),
        ADMIN("ADMINISTRATOR");

        private final String name;

        Role(String name){
                this.name = name;
        }

        public String displayName(){
                return name;
        }

}
