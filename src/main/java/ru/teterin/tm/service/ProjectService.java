package ru.teterin.tm.service;

import ru.teterin.tm.entity.Project;
import ru.teterin.tm.entity.Task;
import ru.teterin.tm.error.IncorrectDateException;
import ru.teterin.tm.error.IncorrectObject;
import ru.teterin.tm.error.IncorrectUuidException;
import ru.teterin.tm.error.ObjectNotFoundException;
import ru.teterin.tm.repository.ProjectRepository;
import ru.teterin.tm.repository.TaskRepository;
import ru.teterin.tm.util.DateUuidParseUtil;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class ProjectService {

        private final ProjectRepository projectRepository;

        private final TaskRepository taskRepository;

        public ProjectService(ProjectRepository projectRepository, TaskRepository taskRepository) {
                this.projectRepository = projectRepository;
                this.taskRepository = taskRepository;
        }

        public Project findOne(String userId, String id) {
                Project project;
                if (id == null || id.isEmpty() || userId == null || userId.isEmpty()) {
                        throw new IncorrectUuidException();
                }
                project = projectRepository.findOne(DateUuidParseUtil.toUUID(userId), DateUuidParseUtil.toUUID(id));
                if (project == null) {
                        throw new ObjectNotFoundException();
                }
                return project;
        }

        public Collection<Project> findAll(String userId) {
                if (userId == null || userId.isEmpty()) {
                        throw new IncorrectUuidException();
                }
                return projectRepository.findAll(DateUuidParseUtil.toUUID(userId));
        }

        // Добавление новой задачи
        public Project persist(String userId, Project project) {
                if (userId == null || userId.isEmpty()) {
                        throw new IncorrectUuidException();
                }
                if (project == null || project.getName().isEmpty() || project.getUserId() == null) {
                        throw new IncorrectObject("Проект введен некоректно!");
                }
                if (!userId.equals(project.getUserId())) {
                        throw new IncorrectObject("Проект другого пользователя!");
                }
                return projectRepository.persist(DateUuidParseUtil.toUUID(userId), project);
        }

        //Добавление/изменение задачи
        public Project merge(String userId, Project project) {
                if (userId == null || userId.isEmpty()) {
                        throw new IncorrectUuidException();
                }
                if (project == null || project.getName().isEmpty() || project.getUserId() == null) {
                        throw new IncorrectObject("Проект введен некоректно!");
                }
                if (!userId.equals(project.getUserId())) {
                        throw new IncorrectObject("Проект другого пользователя!");
                }
                return projectRepository.merge(DateUuidParseUtil.toUUID(userId), project);
        }

        public Project remove(String userId, String id) {
                Project project;
                if (id == null || id.isEmpty() || userId == null || userId.isEmpty()) {
                        throw new IncorrectUuidException();
                }
                id = DateUuidParseUtil.toUUID(id);
                userId = DateUuidParseUtil.toUUID(userId);
                project = projectRepository.remove(userId, id);
                if (project == null) {
                        throw new ObjectNotFoundException();
                }
                taskRepository.removeAllTaskByProjectId(userId, id);
                return project;
        }

        public void removeAll(String userId) {
                if (userId == null || userId.isEmpty()) {
                        throw new IncorrectUuidException();
                }
                userId = DateUuidParseUtil.toUUID(userId);
                taskRepository.removeAll(userId);
                projectRepository.removeAll(userId);
        }

        public Project createProject(String userId, String name, String description, String startDate, String endDate) {
                boolean noStartDate;
                boolean noEndDate;
                if (userId == null || userId.isEmpty()) {
                        throw new IncorrectUuidException();
                }
                if (name == null || name.isEmpty()) {
                        throw new IncorrectObject("Проект введен некоректно!");
                }
                if (description == null) {
                        throw new IncorrectObject("Проект введен некоректно!");
                }
                noStartDate = startDate == null || startDate.isEmpty();
                noEndDate = endDate == null || endDate.isEmpty();
                if (noStartDate || noEndDate) {
                        throw new IncorrectDateException();
                }
                userId = DateUuidParseUtil.toUUID(userId);
                return new Project(userId, name, description, DateUuidParseUtil.toDate(startDate), DateUuidParseUtil.toDate(endDate));
        }

        public Collection<Task> findAllTaskByProjectId(String userId, String id) {
                Project project;
                if (id == null || id.isEmpty() || userId == null || userId.isEmpty()) {
                        throw new IncorrectUuidException();
                }
                id = DateUuidParseUtil.toUUID(id);
                userId = DateUuidParseUtil.toUUID(userId);
                project = findOne(userId, id);
                if (project == null) {
                        throw new ObjectNotFoundException();
                }
                return taskRepository.findAllTaskByProjectId(userId, id);
        }

}
