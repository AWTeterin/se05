package ru.teterin.tm.service;

import ru.teterin.tm.entity.User;
import ru.teterin.tm.error.IncorrectObject;
import ru.teterin.tm.error.IncorrectUuidException;
import ru.teterin.tm.error.ObjectExistException;
import ru.teterin.tm.error.ObjectNotFoundException;
import ru.teterin.tm.repository.UserRepository;
import ru.teterin.tm.util.DateUuidParseUtil;
import ru.teterin.tm.util.HashUtil;

import java.util.Collection;

public class UserService {

        private final UserRepository userRepository;

        public UserService(UserRepository userRepository) {
                this.userRepository = userRepository;
        }

        public Collection<User> findAll(String userId) {
                if (userId == null || userId.isEmpty()) {
                        throw new IncorrectUuidException();
                }
                return userRepository.findAll(DateUuidParseUtil.toUUID(userId));
        }

        public User findOne(String userId, String id) {
                if (userId == null || userId.isEmpty() || id == null || id.isEmpty()) {
                        throw new IncorrectUuidException();
                }
                return userRepository.findOne(DateUuidParseUtil.toUUID(userId), DateUuidParseUtil.toUUID(id));
        }

        public User persist(String userId, User newObject) throws ObjectExistException {
                if (userId == null || userId.isEmpty() || newObject == null || newObject.getLogin() == null) {
                        throw new IncorrectObject("Пользователь введен некоректно!");
                }
                return userRepository.persist(DateUuidParseUtil.toUUID(userId), newObject);
        }

        public User merge(String userId, User object) {
                if (userId == null || userId.isEmpty() || object == null || object.getLogin() == null) {
                        throw new IncorrectObject("Пользователь введен некоректно!");
                }
                return userRepository.merge(DateUuidParseUtil.toUUID(userId), object);
        }

        public User remove(String userId, String id) {
                if (userId == null || userId.isEmpty() || id == null || id.isEmpty()) {
                        throw new IncorrectUuidException();
                }
                return userRepository.remove(DateUuidParseUtil.toUUID(userId), DateUuidParseUtil.toUUID(id));
        }

        public void removeAll(String userId) {
                if (userId == null || userId.isEmpty()) {
                        throw new IncorrectUuidException();
                }
                userRepository.removeAll(DateUuidParseUtil.toUUID(userId));
        }

        public User checkUser(String login, String password) {
                User user;
                boolean incorrectLogin = login == null || login.isEmpty();
                boolean incorrectPassword = password == null || password.isEmpty();
                if (incorrectLogin || incorrectPassword) {
                        throw new IncorrectObject("Пользователь введен некоректно!");
                }
                user = userRepository.findByLogin(login);
                password = HashUtil.md5Custom(password);

                if (user == null || !(password.equals(user.getPassword()))) {
                        throw new ObjectNotFoundException();
                }
                return user;
        }

        public void changePassword(String login, String oldPassword, String newPassword) {
                boolean noLogin = login == null || login.isEmpty();
                boolean noPassword = oldPassword == null || oldPassword.isEmpty() || newPassword == null || newPassword.isEmpty();
                User user;
                if (noLogin || noPassword) {
                        throw new IncorrectObject("Некоректный пароль!");
                }
                user = checkUser(login, oldPassword);
                if (user == null) {
                        throw new IncorrectObject("Неверный пароль!");
                }
                user.setPassword(HashUtil.md5Custom(newPassword));
        }

        public void createUser(String login, String password) {
                boolean noLogin = login == null || login.isEmpty();
                boolean noPassword = password == null || password.isEmpty();
                User user;
                if (noLogin || noPassword) {
                        throw new IncorrectObject("Некоректные логин или пароль!");
                }
                user = userRepository.findByLogin(login);
                if (user != null) {
                        throw new ObjectExistException();
                }
                user = new User(login, HashUtil.md5Custom(password));
                persist(user.getId(), user);
        }

        public void editUser(String oldLogin, String newLogin) {
                User user;
                boolean noLogin = oldLogin == null || oldLogin.isEmpty() || newLogin == null || newLogin.isEmpty();
                if (noLogin) {
                        throw new IncorrectObject("Пустой логин!");
                }
                if (userRepository.findByLogin(newLogin) != null) {
                        throw new ObjectExistException();
                }
                user = userRepository.findByLogin(oldLogin);
                user.setLogin(newLogin);
        }

}
