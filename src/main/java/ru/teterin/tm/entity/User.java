package ru.teterin.tm.entity;

import ru.teterin.tm.enumerated.Role;

import java.util.UUID;

public class User {

        private String id = UUID.randomUUID().toString();

        private String login = "";

        private String password = "";

        private Role role = Role.USER;

        public User() {
        }

        public User(Role role) {
                this.role = role;
        }

        public User(String login, String password) {
                this.login = login;
                this.password = password;
        }

        public User(String login, String password, Role role) {
                this.login = login;
                this.password = password;
                this.role = role;
        }

        public String getLogin() {
                return login;
        }

        public void setLogin(String login) {
                this.login = login;
        }

        public String getPassword() {
                return password;
        }

        public void setPassword(String password) {
                this.password = password;
        }

        public Role getRole() {
                return role;
        }

        public void setRole(Role role) {
                this.role = role;
        }

        public String getId() {
                return id;
        }

        public void setId(String id) {
                this.id = id;
        }

}
