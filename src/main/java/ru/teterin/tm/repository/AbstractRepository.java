package ru.teterin.tm.repository;

import ru.teterin.tm.error.ObjectExistException;

import java.util.Collection;

public abstract class AbstractRepository<T> {

        public abstract Collection<T> findAll(String userId);

        public abstract T findOne(String userId, String id);

        public abstract T persist(String userId, T newObject) throws ObjectExistException;

        public abstract T merge(String userId, T object);

        public abstract T remove(String userId, String id);

        public abstract void removeAll(String userId);

}
