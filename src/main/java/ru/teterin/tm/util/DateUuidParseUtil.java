package ru.teterin.tm.util;

import ru.teterin.tm.error.IncorrectDateException;
import ru.teterin.tm.error.IncorrectUuidException;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.UUID;

public class DateUuidParseUtil {

        public static final DateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy");

        public static String toUUID(String id) {
                try {
                        return UUID.fromString(id).toString();
                } catch (IllegalArgumentException e) {
                        throw new IncorrectUuidException();
                }
        }

        public static Date toDate(String stringDate) {
                try {
                        return DATE_FORMAT.parse(stringDate);
                } catch (ParseException e) {
                        throw new IncorrectDateException();
                }
        }

        public static void printCollection(Collection collection) {
                int count = 1;
                for (Object object : collection) {
                        System.out.println(count + ". " + object);
                        count++;
                }
        }

}
