package ru.teterin.tm.command.task;

import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.util.DateUuidParseUtil;

public class TaskListCommand extends AbstractCommand {

        @Override
        public String getName() {
                return "task-list";
        }

        @Override
        public String getDescription() {
                return "Show all tasks.";
        }

        @Override
        public void execute() {
                System.out.println("[TASK LIST]");
                DateUuidParseUtil.printCollection(bootstrap.getTaskService().findAll(bootstrap.getStateService().getUserId()));
                System.out.println("[OK]");
        }

        @Override
        public boolean secure() {
                return true;
        }

}
