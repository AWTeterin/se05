package ru.teterin.tm.command.task;

import ru.teterin.tm.command.AbstractCommand;

public class TaskCreateCommand extends AbstractCommand {

        @Override
        public String getName() {
                return "task-create";
        }

        @Override
        public String getDescription() {
                return "Create new task for project.";
        }

        @Override
        public void execute() {
                String userId = bootstrap.getStateService().getUserId();
                String projectId;
                String name;
                String description;
                String startDate;
                String endDate;
                System.out.println("[TASK CREATE]");
                System.out.println("ENTER NAME:");
                name = scanner.nextLine();
                System.out.println("ENTER PROJECT ID:");
                projectId = scanner.nextLine();
                System.out.println("ENTER DESCRIPTION:");
                description = scanner.nextLine();
                System.out.println("ENTER START DATE:");
                startDate = scanner.nextLine();
                System.out.println("ENTER END DATE:");
                endDate = scanner.nextLine();
                bootstrap.getTaskService().persist(userId, bootstrap.getTaskService().createTask(userId, projectId, name, description, startDate, endDate));
                System.out.println("[OK]");
        }

        @Override
        public boolean secure() {
                return true;
        }

}
