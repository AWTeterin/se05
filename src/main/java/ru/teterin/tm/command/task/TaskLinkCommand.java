package ru.teterin.tm.command.task;

import ru.teterin.tm.command.AbstractCommand;

public class TaskLinkCommand extends AbstractCommand {

        @Override
        public String getName() {
                return "task-link";
        }

        @Override
        public String getDescription() {
                return "Link an task to a project.";
        }

        @Override
        public void execute() {
                String projectId;
                String taskId;
                System.out.println("[TASK LINK]");
                System.out.println("ENTER PROJECT ID:");
                projectId = scanner.nextLine();
                System.out.println("ENTER TASK ID:");
                taskId = scanner.nextLine();
                bootstrap.getTaskService().linkTask(bootstrap.getStateService().getUserId(), projectId, taskId);
                System.out.println("[OK]");
        }

        @Override
        public boolean secure() {
                return true;
        }

}
