package ru.teterin.tm.command.task;

import ru.teterin.tm.command.AbstractCommand;

public class TaskClearCommand extends AbstractCommand {

        @Override
        public String getName() {
                return "task-clear";
        }

        @Override
        public String getDescription() {
                return "Remove all task.";
        }

        @Override
        public void execute() {
                System.out.println("[TASK CLEAR]");
                bootstrap.getTaskService().removeAll(bootstrap.getStateService().getUserId());
                System.out.println("[ALL TASKS REMOVED]");
        }

        @Override
        public boolean secure() {
                return true;
        }

}
