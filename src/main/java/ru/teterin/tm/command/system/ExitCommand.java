package ru.teterin.tm.command.system;

import ru.teterin.tm.command.AbstractCommand;

public class ExitCommand extends AbstractCommand {

        @Override
        public String getName() {
                return "exit";
        }

        @Override
        public String getDescription() {
                return "Close application.";
        }

        @Override
        public void execute() {
                System.exit(0);
        }

        @Override
        public boolean secure() {
                return false;
        }

}
