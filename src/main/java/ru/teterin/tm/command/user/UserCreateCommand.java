package ru.teterin.tm.command.user;

import ru.teterin.tm.command.AbstractCommand;

public class UserCreateCommand extends AbstractCommand {

        @Override
        public String getName() {
                return "user-create";
        }

        @Override
        public String getDescription() {
                return "Create new user.";
        }

        @Override
        public void execute() {
                String login;
                String password;
                System.out.println("[CREATE USER]");
                System.out.println("ENTER LOGIN:");
                login = scanner.nextLine();
                System.out.println("ENTER PASSWORD:");
                password = scanner.nextLine();
                bootstrap.getUserService().createUser(login, password);
                System.out.println("[OK]");
        }

        @Override
        public boolean secure() {
                return false;
        }

}
