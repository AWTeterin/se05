package ru.teterin.tm.command.user;

import ru.teterin.tm.command.AbstractCommand;

public class UserLogOutCommand extends AbstractCommand {

        @Override
        public String getName() {
                return "user-logout";
        }

        @Override
        public String getDescription() {
                return "Log out to the task manager.";
        }

        @Override
        public void execute() {
                System.out.println("[USER LOGOUT]");
                System.out.println("BYE BYE " + bootstrap.getStateService().getLogin());
                bootstrap.getStateService().setUserId(null);
                bootstrap.getStateService().setLogin(null);
                bootstrap.getStateService().setRole(null);
                bootstrap.getStateService().setProjectId(null);
                System.out.println("[OK]");
        }

        @Override
        public boolean secure() {
                return true;
        }

}
