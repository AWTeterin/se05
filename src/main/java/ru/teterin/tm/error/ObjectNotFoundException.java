package ru.teterin.tm.error;

public class ObjectNotFoundException extends RuntimeException {

        @Override
        public void printStackTrace() {
                System.out.println("Объект не найден!");
        }

}
