package ru.teterin.tm.error;

public class ObjectExistException extends RuntimeException {

        @Override
        public void printStackTrace() {
                System.out.println("Объект уже существует!");
        }

}
